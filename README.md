这是yx2008班级的远程仓库

远程仓库的地址:https://gitee.com/lmx1989/yx2008.git

远程操作:
		注册账号:
			github
				github.com
			gitee(码云)
				gitee.com
			这两种远程仓库的注册方式,几乎没有查询
				github是英文的
				gitee是中文的
		
		远程仓库地址: https://gitee.com/lmx1989/yx2008.git

		命令:
			关联远程仓库,可以关联多个远程仓库
				git remote add 远程仓库名称 远程仓库地址
					git remote add origin https://gitee.com/lmx1989/yx2008.git
			显示关联的远程仓库
				git remote -v
			删除关联的远程地址:
				git remote remove 远程仓库名称

			将本地仓库的代码推送到远程仓库
				第一次推送的时候,会让大家登录我们的git的账号.
					注册github登录或gitee登录的账号
				git push 远程仓库名称 本地分支名称
			删除远程分支
				git push -d 远程仓库名称 远程仓库分支
			如何对远程仓库的文件进行增删改操作呢?
				首先对本地仓库进行增删改,然后推送到远程仓库即可

			克隆远程仓库的代码到本地:
				将远程仓库的代码和.git一起克隆到本地
				git clone 远程仓库地址
					克隆默认的分支
				git clone -b day01 https://gitee.com/lmx1989/yx2008.git 
					克隆选中的分支
			拉取代码
				git pull 远程仓库名称 远程仓库分支:本地仓库分支
					拉取指定分支的代码
				通常拉取master分支
				拉取默认分支的代码
